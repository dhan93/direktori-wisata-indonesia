<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpCreatedEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|unique:users,email|email',
        'username' => 'required|unique:users,username'
      ]);

      if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
      }

      $user = User::create($request->all());

      do {
        $random = mt_rand(100000, 999999);

        $check = OtpCode::where('otp', $random)->first();
      } while ($check);

      $now = Carbon::now();

      $otp_code = OtpCode::create([
        'otp' => $random,
        'user_id' => $user->id,
        'valid_until' => $now->addMinutes(5)
      ]);

      event(new OtpCreatedEvent($otp_code));

      return response()->json([
        'success' => true,
        'message' => 'Data user berhasil dibuat',
        'data' => [
          'user' => [
            'email' => $user->email,
            'username' => $user->username,
            'name' => $user->name
          ],
          // 'otp_code' => $otp_code
        ]
      ]);
    }
}
