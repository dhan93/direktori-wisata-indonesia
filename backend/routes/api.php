<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api', 'prefix' => 'auth', 'namespace' => 'Auth'], function ($router) {
  Route::post('login', 'AuthController@login');
  Route::post('logout', 'AuthController@logout');
  Route::post('refresh', 'AuthController@refresh');
  Route::post('me', 'AuthController@me');
});

// // Uncomment setelah controller dibuat
Route::prefix('auth')->namespace('Auth')->group(function () {
  Route::post('register', 'RegisterController')->name('auth.register');    
  Route::post('regenerate-otp-code', 'RegenerateOtpcodeController')->name('auth.regenerate_otp_code');    
  Route::post('verification', 'VerificationController')->name('auth.varification');    
  Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
});

Route::group(['middleware' => 'api'], function (){
  Route::apiResource('category', 'CategoryController')->except(['index', 'show']);
  Route::apiResource('object', 'ObjectController')->except(['index', 'show']);
  Route::apiResource('review', 'ReviewController')->except(['index', 'show']);
  Route::apiResource('track', 'TrackController')->except(['index', 'show']);
});

Route::apiResource('category', 'CategoryController')->only(['index', 'show']);
Route::apiResource('object', 'ObjectController')->only(['index', 'show']);
Route::get('provinces', 'ProvinceController')->name('province');
Route::get('cities/{provinceId}', 'CityController')->name('city');