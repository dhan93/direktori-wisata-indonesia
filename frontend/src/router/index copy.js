import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "about" */ '../views/HomeView.vue')
  },
  {
    path: '/categories',
    name: 'kategori',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/KategoriView.vue')
  },
  {
    path: '/Objects',
    name: 'Objek',
    component: () => import(/* webpackChunkName: "about" */ '../views/ObjekView.vue')
  },
  {
    path: '/user',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "about" */ '../views/ProfileView.vue')
  },
  {
    path: '/review',
    name: 'Review',
    component: () => import(/* webpackChunkName: "about" */ '../views/ReviewView.vue')
  },
  {
    path: '/track',
    name: 'rute',
    component: () => import(/* webpackChunkName: "about" */ '../views/TrackView.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
