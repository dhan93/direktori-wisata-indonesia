<?php

namespace App\Http\Controllers;

use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TrackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // reject update current user not admin
      if (auth()->user()->role != 'admin') {
        return response()->json([
          'success' => false,
          'message' => 'unauthorized action',
        ], 403);
      }

      $validator = Validator::make($request->all(), [
        'object_id' => 'required|exists:objects,id',
        'steps' => 'required'
      ]);

      //response error validation
      if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
      }

      $track = Track::create([
        'object_id' => $request->object_id,
        'steps' => $request->steps
      ]);

      if($track) {
        return response()->json([
            'success' => true,
            'message' => 'Track added',
            'data'    => $track
        ], 200);
      } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // reject update current user not admin
      if (auth()->user()->role != 'admin') {
        return response()->json([
          'success' => false,
          'message' => 'unauthorized action',
        ], 403);
      }
      
      $validator = Validator::make($request->all(), [
        'steps' => 'required'
      ]);

      //response error validation
      if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
      }

      $track = Track::findOrFail($id);
      
      $track->update([
        'steps' => $request->steps
      ]);

      if($track) {
        return response()->json([
            'success' => true,
            'message' => 'Track updated',
            'data'    => $track
        ], 200);
      } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // reject update current user not admin
      if (auth()->user()->role != 'admin') {
        return response()->json([
          'success' => false,
          'message' => 'unauthorized action',
        ], 403);
      }

      $track = Track::findOrfail($id);

      if($track) {
          //delete post
          $track->delete();

          return response()->json([
              'success' => true,
              'message' => 'Route Deleted',
          ], 200);
      }

      //data post not found
      return response()->json([
          'success' => false,
          'message' => 'Route Not Found',
      ], 404);
    }
}
