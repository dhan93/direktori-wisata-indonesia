import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/category',
    name: 'Kategori',
    component: () => import('../views/Category/IndexView.vue')
  },
  {
    path: '/category/:id',
    name: 'Category Show',
    component: () => import('../views/Category/ShowView.vue')
  },
  {
    path: '/Object',
    name: 'Objek',
    component: () => import('../views/Object/IndexView.vue')
  },
  {
    path: '/Object/:id',
    name: 'Objek Show',
    component: () => import('../views/Object/ShowView.vue')
  },
  {
    path: '/user',
    name: 'Profile',
    component: () => import('../views/User/ProfileView.vue')
  },
  {
    path: '/review',
    name: 'Review',
    component: () => import('../views/ReviewView.vue')
  },
  {
    path: '/track',
    name: 'rute',
    component: () => import('../views/TrackView.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
