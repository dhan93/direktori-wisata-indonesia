<?php

namespace App\Http\Controllers;

use App\Province;

class ProvinceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
      $provinces = Province::orderBy('name')->get();
      return response()->json([
        'data'    => $provinces
      ], 200);
    }
}
