<?php

namespace App\Http\Controllers;

use App\Review;
use App\TourObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $reviews = Review::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data Review',
            'data'    => $reviews  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $review = Review::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Review',
            'data'    => $review 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'content' => 'required'
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $review = Review::create([
            'title'     => $request->title,
            'content'   => $request->content,
            'rating'    => $request->rating,
            'object_id' => $request->object_id,
            'user_id'   => auth()->id()
        ]);

        // update tour object's average rating
        $object = TourObject::find($request->object_id);
        $ratings = Review::where('object_id', $request->object_id)->pluck('rating')->toArray();
        
        $object->update([
          'average_rating' => array_sum($ratings)/count($ratings)
        ]);

        //success save to database
        if($review) {

            return response()->json([
                'success' => true,
                'message' => 'Review Created',
                'data'    => $review
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Review Failed to Post',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'content' => 'required',
            'rating'  => 'in:1,2,3,4,5',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $review = Review::findOrFail($id);
        

        // reject update if post not created by current user
        if ($review->user_id != auth()->id()) {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }
        
        if($review) {
          
            //update post
            $review->update([
                'title'     => $request->title,
                'content'   => $request->content,
                'rating'  => $request->rating
            ]);
            
            // update tour object's average rating
            $object = TourObject::find($review->object_id);
            $object->update([
              'average_rating' => Review::where('object_id', $review->object_id)->avg('rating')
            ]);
            
            return response()->json([
                'success' => true,
                'message' => 'Review Updated',
                'data'    => $review  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Review Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $review = Review::findOrfail($id);

        // reject update if post not created by current user
        if ($review->user_id != auth()->id() && auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }

        if($review) {
            //delete child rows
            DB::table('file_review')->where('review_id', $id)->delete();
            //delete post
            $review->delete();

            return response()->json([
                'success' => true,
                'message' => 'Review Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Review Not Found',
        ], 404);
    }
}