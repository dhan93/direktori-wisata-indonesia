<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Rating extends Model
{
  protected $fillable = ['object_id', 'user_id', 'rating'];
  protected $primaryKey = 'id';
  protected $keyType = 'string';
  public $incrementing = false;
  
  protected static function boot()
  {
    parent::boot();

    static::creating( function($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }

  public function users()
  {
    return $this->belongsToMany('App\User');
  }

  public function object()
  {
    return $this->belongsTo('App\TourObject');
  }
}
