<?php

namespace App\Http\Controllers;

use App\City;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke($provinceId)
    {
      $cities = City::where('province_id', $provinceId)
        ->orderBy('name')
        ->get();
      return response()->json([
        'data'    => $cities
      ], 200);
    }
}
