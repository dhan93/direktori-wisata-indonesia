<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $categories = Category::all();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Kategori',
            'data'    => $categories  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $category = Category::where('id',$id)
          ->with('objects.city')
          ->first();
        return response($category);
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Kategori',
            'data'    => $category 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }

        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required|unique:categories,name',            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $category = Category::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($category) {

            return response()->json([
                'success' => true,
                'message' => 'Kategori Created',
                'data'    => $category  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Kategori Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Category $category)
    {
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }

        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $category = Category::findOrFail($category->id);

        if($category) {

            //update post
            $category->update([
                'name'     => $request->name,
              
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Kategori Updated',
                'data'    => $category  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }
        
        //find post by ID
        $category = Category::findOrfail($id);

        if($category) {

            //delete post
            $category->delete();

            return response()->json([
                'success' => true,
                'message' => 'Kategori Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Kategori Not Found',
        ], 404);
    }
}