<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $now = Carbon::now();
      DB::table('users')->insert([
        'id' => Str::uuid(),
        'name' => 'The Admin',
        'username' => 'adminIsCool',
        'email' => 'admin@admin.com',
        'password' => bcrypt('password'),
        'role' => 'admin',
        'email_verified_at' => $now,
        'created_at' => $now,
        'updated_at' => $now
      ]);
    }
}
