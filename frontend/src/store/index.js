import Vue from 'vue'
import Vuex from 'vuex'
import dialog from './dialog'
import auth from './auth'
import VuexPersists from 'vuex-persist'

const vuexPersists = new VuexPersists({
  key: 'dwi',
  storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersists.plugin],
  modules: {
    dialog,
    auth
  }
})
