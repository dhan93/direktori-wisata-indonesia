<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TourObject extends Model
{
  protected $fillable = ['name', 'content', 'province_id', 'city_id', 'average_rating', 'featured_image'];
  protected $table = 'objects';
  protected $primaryKey = 'id';
  protected $keyType = 'string';
  public $incrementing = false;
  
  protected static function boot()
  {
    parent::boot();

    static::creating( function($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }

  public function categories()
  {
    return $this->belongsToMany('App\Category', 'category_object', 'object_id', 'category_id');
  }

  public function city()
  {
    return $this->belongsTo('App\City');
  }

  public function province()
  {
    return $this->belongsTo('App\Province');
  }

  public function tracks()
  {
    return $this->hasMany('App\Track', 'object_id', 'id');
  }

  public function reviews()
  {
    return $this->hasMany('App\Review', 'object_id', 'id');
  }

}
