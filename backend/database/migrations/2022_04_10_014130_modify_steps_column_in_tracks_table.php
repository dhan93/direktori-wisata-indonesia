<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModifyStepsColumnInTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // DB::statement("ALTER TABLE tracks ALTER COLUMN steps TYPE TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
      Schema::table('tracks', function (Blueprint $table) {
        $table->dropColumn('steps');
      });
      Schema::table('tracks', function (Blueprint $table) {
        $table->text('steps');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracks', function (Blueprint $table) {
            //
        });
    }
}
