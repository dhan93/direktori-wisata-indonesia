import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
// import { mapGetters, mapMutations } from 'vuex'

Vue.config.productionTip = false
Vue.prototype.$apiDomain = 'https://project2-dw-indonesia.herokuapp.com/api'
Vue.prototype.$mediaDomain = 'https://project2-dw-indonesia.herokuapp.com/uploads/'
// Vue.prototype.$apiDomain = 'http://localhost:8000/api'
// Vue.prototype.$mediaDomain = 'http://localhost:8000/uploads/'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
