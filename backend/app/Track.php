<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Track extends Model
{
  protected $fillable = ['object_id', 'steps'];
  protected $primaryKey = 'id';
  protected $keyType = 'string';
  public $incrementing = false;

  protected static function boot()
  {
    parent::boot();

    static::creating( function($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }

  public function object()
  {
    return $this->belongsTo('App\TourObject');
  }
}
