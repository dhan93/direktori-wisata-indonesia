<?php

namespace App\Http\Controllers;

use App\Review;
use App\TourObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ObjectController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $objects = TourObject::with(['categories', 'city'])
          ->withCount('reviews')
          ->orderBy('updated_at', 'desc')
          ->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Objek',
            'data'    => $objects,
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $object = TourObject::with(['province', 'city', 'reviews.user', 'tracks', 'categories'])
          ->find($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Objek',
            'data'    => [
              "object"=>$object
              ]
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }
        
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'content' => 'required',
            'province_id' => 'required|exists:provinces,id',
            'city_id' => 'required|exists:cities,id',
            'featured_image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'exists:categories,id'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $imageName = NULL;
        // store featured image
        if ($request->featured_image) {
          $imageName = time().'.'.$request->featured_image->extension();
          $request->featured_image->move(public_path('uploads'), $imageName);
        }

        //save to database
        $object = TourObject::create([
            'name'     => $request->name,
            'content'   => $request->content,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'featured_image' => $imageName
        ]);

        if ($request->category_id) {
          $object->categories()->attach($request->category_id);
        }        

        //success save to database
        if($object) {

            return response()->json([
                'success' => true,
                'message' => 'Objek Created',
                'data'    => $object  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Objek Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, $id)
    {
      // return response($request);
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }

        //set validation
        $validator = Validator::make($request->all(), [
          'name'   => 'required',
          'content' => 'required',
          'province_id' => 'required|exists:provinces,id',
          'city_id' => 'required|exists:cities,id',
          'featured_image' => 'image|mimes:jpeg,png,jpg|max:2048',
          'category_id' => 'exists:categories,id'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $object = TourObject::findOrFail($id);

        if($object) {

            $imageName = NULL;
            // store featured image
            if ($request->featured_image) {
              $imageName = time().'.'.$request->featured_image->extension();
              $request->featured_image->move(public_path('uploads'), $imageName);
            }

            //update post
            $object->update([
              'name'     => $request->name,
              'content'   => $request->content,
              'province_id' => $request->province_id,
              'city_id' => $request->city_id,
              'featured_image' => $imageName
            ]);

            if ($request->category_id) {
              $object->categories()->detach();
              $object->categories()->attach($request->category_id);
            }      

            return response()->json([
                'success' => true,
                'message' => 'Objek Updated',
                'data'    => $object  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Objek Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        // reject update current user not admin
        if (auth()->user()->role != 'admin') {
          return response()->json([
            'success' => false,
            'message' => 'unauthorized action',
          ], 403);
        }

        //find post by ID
        $object = TourObject::findOrfail($id);

        if($object) {

            //delete child rows
            DB::table('reviews')->where('object_id', $id)->delete();
            DB::table('category_object')->where('object_id', $id)->delete();
            DB::table('file_object')->where('object_id', $id)->delete();
            DB::table('tracks')->where('object_id', $id)->delete();
            //delete post
            $object->delete();

            return response()->json([
                'success' => true,
                'message' => 'Objek Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Objek Not Found',
        ], 404);
    }
}