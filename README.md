## About Direktori Wisata Indonesia

Dibuat sebagai pemenuhan tugas project 1 dari kelas Full Stack Web Dev PKS Digital School Batch 2.
Anggota tim 7 dari grup Telegram 2 terdiri atas:

- [Firdaus M](https://gitlab.com/muhfir).
- [Maulana Ulul Azmi](https://gitlab.com/maulanaulul).
- [Pandu](https://gitlab.com/vancisme).
- [Ramadhan](https://gitlab.com/dhan93).

Project ini bertemakan Direktori Wisata Indonesia

### setup
jalankan migrasi dengan seed

login default user:
- email: admin@admin.com
- password: password

ERD : ![erd](https://gitlab.com/dhan93/direktori-wisata-indonesia/-/blob/main/desain/app_erd_rc1.jpg)

Backend deployment: [https://project2-dw-indonesia.herokuapp.com/api](https://project2-dw-indonesia.herokuapp.com/api)

Dokumentasi API: [https://documenter.getpostman.com/view/6516868/UVyxQtx7](https://documenter.getpostman.com/view/6516868/UVyxQtx7)

Frontend deployment: [https://dw-indonesia-project.netlify.app/](https://dw-indonesia-project.netlify.app/)

Video demo dapat diakses di: [Google Drive]()

Screenshot:

