<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\OtpCode;
use App\User;
use Illuminate\Support\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'otp'   => 'required',
      ]);
      
      //response error validation
      if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
      }

      $otp_code = OtpCode::where('otp', $request->otp)->first();

      if (!$otp_code) {
        return response()->json([
          'success'=>false,
          'message'=>'OTP code tidak valid',

        ], 400);
      }

      $now = Carbon::now();

      if ($now > $otp_code->valid_until) {
        return response()->json([
          'success'=>false,
          'message'=>'OTP code expired',

        ], 400);
      }

      $user = User::find($otp_code->user_id);
      $user->update([
        'email_verified_at' => $now,
      ]);

      $user->otp_code->delete();

      return response()->json([
        'success'=>true,
        'message'=>'Verifikasi Berhasil',
        'data'=> $user
      ]);
    }
}
