<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Review extends Model
{
  protected $fillable = ['title', 'content', 'user_id', 'object_id', 'rating'];
  protected $primaryKey = 'id';
  protected $keyType = 'string';
  public $incrementing = false;
  
  protected static function boot()
  {
    parent::boot();

    static::creating( function($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function object()
  {
    return $this->belongsTo('App\TourObject');
  }
}
