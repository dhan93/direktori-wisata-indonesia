<?php

namespace App\Listeners;

use App\Events\OtpCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\OtpGeneratedMail;

class SendOtpToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCreatedEvent  $event
     * @return void
     */
    public function handle(OtpCreatedEvent $event)
    {
      Mail::to($event->otpCode->user->email)->send(new OtpGeneratedMail($event->otpCode));
    }
}
